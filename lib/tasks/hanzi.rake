namespace :hanzi do

  desc 'updates cache counter on box records'
  task :update_counters => :environment do
    Box.all.each do |box|
      ActiveRecord::Base.connection.execute("update boxes set hanzi_chars_count = #{box.hanzi_chars.length} where id = #{box.id}")
    end
  end
end

