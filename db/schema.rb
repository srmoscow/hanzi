# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130616165947) do

  create_table "boxes", :force => true do |t|
    t.string   "name"
    t.integer  "percentage_to_take"
    t.integer  "hanzi_chars_count",  :default => 0
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  create_table "flashcard_sets", :force => true do |t|
    t.integer  "num_cards"
    t.integer  "num_tested"
    t.string   "groups"
    t.text     "serialized_cards"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "flashcard_sets_groups", :id => false, :force => true do |t|
    t.integer "flashcard_set_id"
    t.integer "group_id"
  end

  add_index "flashcard_sets_groups", ["flashcard_set_id", "group_id"], :name => "index_flashcard_sets_groups_on_flashcard_set_id_and_group_id"
  add_index "flashcard_sets_groups", ["group_id", "flashcard_set_id"], :name => "index_flashcard_sets_groups_on_group_id_and_flashcard_set_id"

  create_table "grouped_hanzis", :force => true do |t|
    t.integer  "hanzi_char_id"
    t.integer  "group_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "hanzi_chars", :force => true do |t|
    t.string   "character"
    t.string   "pinyin"
    t.text     "meaning"
    t.integer  "strokes"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.text     "mnemonic"
    t.integer  "box_id",          :default => 1
    t.string   "sortable_pinyin"
    t.text     "tone_hint"
  end

  add_index "hanzi_chars", ["box_id"], :name => "index_hanzi_chars_on_box_id"
  add_index "hanzi_chars", ["character"], :name => "index_hanzi_chars_on_character", :unique => true
  add_index "hanzi_chars", ["pinyin"], :name => "index_hanzi_chars_on_pinyin"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

end
