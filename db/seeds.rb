# encoding: UTF-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)




# == Schema Information
#
# Table name: hanzi_chars
#
#  id         :integer          not null, primary key
#  character  :string(255)
#  pinyin     :string(255)
#  meaning    :text
#  strokes    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null





# HanziChar.delete_all
# HanziChar.create(
#     [
#       {character: '我', pinyin: 'wov', meaning: 'I, me', strokes: 7},
#       {character: '很', pinyin: 'henv', meaning: 'very', strokes: 9},
#       {character: '好', pinyin: 'hao3', meaning: 'good', strokes: 9},
#       {character: '请', pinyin: 'qingv', meaning: 'please (do something)', strokes: 10}
#     ]
#   )






