class AddIndexesToHanziChars < ActiveRecord::Migration
  def change
    add_index :hanzi_chars, :character, :unique => true
    add_index :hanzi_chars, :pinyin, :unique => false
  end
end
