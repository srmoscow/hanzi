class AddBoxIdtoHanziChars < ActiveRecord::Migration
  def up
    add_column :hanzi_chars, :box_id, :integer, :default => 1
    add_index :hanzi_chars, :box_id
  end

  def down
    remove_index :hanzi_chars, :box_id
    remove_column :hanzi_chars, :box_id
  end
end
