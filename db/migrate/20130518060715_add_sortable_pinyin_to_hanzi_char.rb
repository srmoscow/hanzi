class AddSortablePinyinToHanziChar < ActiveRecord::Migration
  def change
    add_column :hanzi_chars, :sortable_pinyin, :string
  end
end
