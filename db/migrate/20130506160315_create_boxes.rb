class CreateBoxes < ActiveRecord::Migration
  def self.up
    create_table :boxes do |t|
      t.string :name
      t.integer :percentage_to_take
      t.integer :hanzi_chars_count, :default => 0

      t.timestamps
    end

    Box.reset_column_information

    seeds = {1 => ['Never Remembered', 50], 2 => ['1x remembered', 25], 3 => ['2x remembered', 10], 4 => ['3x remembered', 10], 5 => ['4x remembered', 5] }
    seeds.each do |record_id, seed_data|
      name, percentage_to_take = seed_data
      box = Box.new(:name => name, :percentage_to_take => percentage_to_take)
      box.id = record_id
      box.save!
    end

  end



  def self.down
    drop_table :boxes
  end


end
  