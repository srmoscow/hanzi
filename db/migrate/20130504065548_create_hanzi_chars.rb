class CreateHanziChars < ActiveRecord::Migration
  def change
    create_table :hanzi_chars do |t|
      t.string :character
      t.string :pinyin
      t.text :meaning
      t.integer :strokes

      t.timestamps
    end
  end
end
