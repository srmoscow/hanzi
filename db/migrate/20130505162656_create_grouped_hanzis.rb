class CreateGroupedHanzis < ActiveRecord::Migration
  def change
    create_table :grouped_hanzis do |t|
      t.integer :hanzi_char_id
      t.integer :group_id

      t.timestamps
    end
  end
end
