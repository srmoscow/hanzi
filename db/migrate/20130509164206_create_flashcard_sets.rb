class CreateFlashcardSets < ActiveRecord::Migration
  def change
    create_table :flashcard_sets do |t|
      t.integer :num_cards
      t.integer :num_tested
      t.string  :groups
      t.text    :serialized_cards

      t.timestamps
    end
  end
end
