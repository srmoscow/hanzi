class CreateFlashcardSetsGroupsTable < ActiveRecord::Migration
  def up
    create_table :flashcard_sets_groups, :id => false do |t|
        t.references :flashcard_set
        t.references :group
    end
    add_index :flashcard_sets_groups, [:flashcard_set_id, :group_id]
    add_index :flashcard_sets_groups, [:group_id, :flashcard_set_id]
  end

  def down
    drop_table :flashcard_sets_groups
  end
end



