class AssignGroups < ActiveRecord::Migration
  def up
    group_names = [ 'Default', 'Lesson 3', 'Lesson 4', 'Names', 'Radicals']
    group_names.each { |group_name| Group.create!(:name => group_name) }
    default_group = Group.find_by_name('Default')
    HanziChar.all.each { |hc| hc.groups << default_group }
  end

  def down
    HanziChar.all.each { |hc| hc.groups = [] }
    Group.delete_all
  end
end
