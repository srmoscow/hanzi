class RemoveDefaultGroup < ActiveRecord::Migration
  def up
    default_group = Group.find_by_name('Default')
    HanziChar.all.each do |hc|
      hc.groups.delete(default_group)
      hc.save!
    end
    default_group.destroy

  end

  def down
  end
end
