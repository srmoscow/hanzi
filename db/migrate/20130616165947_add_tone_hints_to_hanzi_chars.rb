class AddToneHintsToHanziChars < ActiveRecord::Migration
  def change
    add_column :hanzi_chars, :tone_hint, :text
  end
end
