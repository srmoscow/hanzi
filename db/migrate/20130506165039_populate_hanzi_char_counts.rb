class PopulateHanziCharCounts < ActiveRecord::Migration
  def up
    Box.reset_column_information

    Box.all.each do |box|
      box.reload
      box.hanzi_chars_count = box.hanzi_chars.length
      box.save!
    end
  end

  def down
    Box.all.each do |box|
      box.hanzi_chars_count = 0
      box.save!
    end
  end
end
