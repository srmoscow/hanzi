class AddMnemonicToHanziChar < ActiveRecord::Migration
  def change
    add_column :hanzi_chars, :mnemonic, :text
  end
end
