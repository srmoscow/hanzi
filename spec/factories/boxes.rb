# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :box_1, class: Box do
    id 1
    name "Never Remembered"
    percentage_to_take 50

    factory :box_2 do
      id 2
      name "1x rembered"
      percentage_to_take 25
    end

    factory :box_3 do
      id 3
      name "2x rembered"
      percentage_to_take 10
    end  

    factory :box_4 do
      id 4
      name "3x rembered"
      percentage_to_take 10
    end  


    factory :box_5 do
      id 5
      name "4x rembered"
      percentage_to_take 5
    end  
  end
end
