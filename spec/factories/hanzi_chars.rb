# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hanzi_char do
    sequence(:character)    { |n| "char#{n}" }
    sequence(:pinyin)       { |n| "ma-" }
    sequence(:meaning)      { |n| "meaning #{n}" }
    mnemonic                "Remember this"
    box_id                  1
    strokes                 1
  end
end
