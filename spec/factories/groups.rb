# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :group do
    sequence(:name)    { |n| "Group_#{n}"}

    factory :default_group do
      name 'Default'
    end
  end
end

