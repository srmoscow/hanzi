# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :grouped_hanzi do
    character_id 1
    group_id 1
  end
end
