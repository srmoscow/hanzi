# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :flashcard_set do
    num_cards 1
    num_tested 1
    cards "MyString"
  end
end
