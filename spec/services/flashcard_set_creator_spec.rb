require 'spec_helper'

describe FlashcardSetCreator do

  describe '.new' do

    let(:params) { HashWithIndifferentAccess.new( {"num_cards"=>"20", "group_ids"=>["15", ""]} )  }


    it 'should be valid if no groups sepecified' do
      params[:group_ids] = [""]
      creator = FlashcardSetCreator.new(params)
      creator.valid?.should be_true
    end

    
    it 'should remove empty group_ids' do
      params[:group_ids] = ["15", "16", ""]

      set_double = double FlashcardSet
      Box.should_receive(:get_flashcard_ids).with(20, [15, 16]).and_return([1,2,3,4])
      FlashcardSet.should_receive(:new_from_ids).with([1,2,3,4]).and_return(set_double)
      set_double.should_receive(:save!)

      creator = FlashcardSetCreator.new(params)
      creator.valid?.should be_true
      creator.instance_variable_get(:@group_ids).should == [15, 16]
    end





    it 'should not be valid if num_cards not specified' do
      params[:num_cards] = ""
      creator = FlashcardSetCreator.new(params)
      creator.valid?.should be_false
      creator.errors.include?('You must specify a number of cards to pick').should be_true
    end
  end
end