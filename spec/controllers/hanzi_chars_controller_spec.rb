require 'spec_helper'

describe HanziCharsController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      hc = FactoryGirl.create :hanzi_char
      get 'show', id: hc.id
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "returns http success" do
      post 'create'
      response.should be_redirect
    end
  end

  describe "GET 'edit'" do
    it "returns http success" do
      hc = FactoryGirl.create :hanzi_char
      get 'edit', id: hc.id
      response.should be_success
    end
  end

  describe "PUT 'update'" do
    it "returns http success" do
      hc = FactoryGirl.create :hanzi_char
      put 'update', id: hc.id
      response.should be_redirect
    end
  end

  pending describe "GET 'destroy'" do
    it "returns http success" do
      get 'destroy'
      response.should be_success
    end
  end

end
