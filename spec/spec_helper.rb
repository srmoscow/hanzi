# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] = 'test'
RSPEC_ROOT = File.dirname(__FILE__)

require 'simple_cov_enabler'

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'shoulda-matchers'
require 'factory_girl'
require 'awesome_print'
# require 'view_spec_helper'
# require 'controller_spec_helper'
# require 'cancan/matchers'

# FileUtils.mkdir_p "#{::Rails.root}/tmp/nephos_cache"

FactoryGirl.sequences.clear
FactoryGirl.factories.clear
FactoryGirl.find_definitions

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|

  # # turn off garbage collection when running tests
  # # results in 10-20% faster specs
  # config.before(:all) do
  #    DeferredGarbageCollection.start
  # end

  # config.after(:all) do
  #    DeferredGarbageCollection.reconsider
  # end


  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"

  if File.exist?("#{Rails.root}/do_not_use_transactional_fixtures")
    puts "\n++++++++ TRANSACTIONAL FIXTURES OFF ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    config.use_transactional_fixtures = false
    config.use_transactional_examples = false
  end
  
    
  
  config.before(:suite) do
    # Check if the DB is empty before we start, and bail out if it isn't without continuing.
    # Putting db/variable/factory stuff outside of it or let will cause the DB to be
    # populated with that data as the tests are loaded, so it will already be in the DB
    # when this runs. This will prevent you running tests if you have this kind of
    # dangerous stuff in place.
    if config.use_transactional_fixtures
      #temporarily switch on fail_fast, so no tests run if db isn't empty
      prev_fail_fast = config.fail_fast
      config.fail_fast = true
      db_should_be_empty(config.reporter)
      config.fail_fast = prev_fail_fast
    end
  end
  
  config.after(:suite) do
    # We also check the DB is empty after we finish, just in case something has somehow
    # managed to slip outside of a transaction.
    db_should_be_empty(config.reporter) if config.use_transactional_fixtures

    # Flush test redis server
    # Redis.new(YAML.load_file("#{Rails.root}/config/redis.yml")['test']).flushdb
  end

end

def db_should_be_empty(reporter)
  RSpec::Core::ExampleGroup.describe 'entire test suite' do
    it 'should have an empty database before it starts and after it finishes' do
      unempty_klasses = []
      ActiveRecord::Base.subclasses.each do |klass|
        unempty_klasses << klass.to_s unless klass.count == 0
      end
      unempty_klasses.should == []
    end
  end.run(reporter)
end
  
