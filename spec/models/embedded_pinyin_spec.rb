# encoding: UTF-8

require 'spec_helper'

describe EmbeddedPinyin do

  it 'should return the string unchanged if no embedded pinyin' do
    text = 'Now is the time for all good men to come to the aid of the party'
    EmbeddedPinyin.new(text).to_s.should == text
  end


  it 'should replace all pinyin words with accented versions' do
    original = "The character 见 :jian\\ represents the verb to see, and 件 （also :jian\\) means item, or -ware, whereas 减 :jianv means alkali or soda."
    expected = "The character 见 jiàn represents the verb to see, and 件 （also jiàn) means item, or -ware, whereas 减 jiǎn means alkali or soda."
    EmbeddedPinyin.new(original).to_s.should == expected
  end


  it 'should ignore words marked as pinyin which are not valid pinyin' do
    original = "The character 见 :jian\\ represents the verb to see, and 件 （also :jix) means item, or -ware, whereas 减 :jianv means alkali or soda."
    expected = "The character 见 jiàn represents the verb to see, and 件 （also jix) means item, or -ware, whereas 减 jiǎn means alkali or soda."
    EmbeddedPinyin.new(original).to_s.should == expected
  end
end

