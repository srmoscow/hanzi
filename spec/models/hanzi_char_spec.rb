# encoding: UTF-8
# == Schema Information
#
# Table name: hanzi_chars
#
#  id              :integer          not null, primary key
#  character       :string(255)
#  pinyin          :string(255)
#  meaning         :text
#  strokes         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  mnemonic        :text
#  box_id          :integer          default(1)
#  sortable_pinyin :string(255)
#


require 'spec_helper'

describe HanziChar do
  

  describe 'before_save' do

    it 'should transform the pinyin' do
      hc = HanziChar.create(
        :character =>  '我',
        :meaning   => 'I, me, my',
        :mnemonic  => 'Oh! :wo\\ is me!',
        :pinyin    => 'wo3',
        :strokes   => 7
        )


      hc.pinyin.should == 'wǒ'
      hc.mnemonic.should == "Oh! wò is me!"
    end
  end


  describe '#to_s' do
    it 'should display the character and pinyin' do
      FactoryGirl.create(:hanzi_char).to_s.should =~ /char\d+ mā/
    end

  end



  context 'methods requiring setup' do

    before(:each) do
      @group1 = FactoryGirl.create :group
      @group2 = FactoryGirl.create :group
      @group3 = FactoryGirl.create :group

      @box_1  = FactoryGirl.create :box_1
      @box_2  = FactoryGirl.create :box_2
      @box_3  = FactoryGirl.create :box_3


      @hc1_g1   = FactoryGirl.create :hanzi_char, box: @box_1, groups: [@group1]
      @hc2_g1g2 = FactoryGirl.create :hanzi_char, box: @box_1, groups: [@group1, @group2]
      @hc3_g1g2 = FactoryGirl.create :hanzi_char, box: @box_1, groups: [@group1, @group2]

      @hc4_g3   = FactoryGirl.create :hanzi_char, box: @box_2, groups: [@group3]
      @hc5_g1g2 = FactoryGirl.create :hanzi_char, box: @box_2, groups: [@group1, @group2]
      @hc6_g1g3 = FactoryGirl.create :hanzi_char, box: @box_2, groups: [@group1, @group3]

      @hc7_g1g3 = FactoryGirl.create :hanzi_char, box: @box_3, groups: [@group1, @group3]
      @hc8_g1g3 = FactoryGirl.create :hanzi_char, box: @box_3, groups: [@group3]
      @hc9_g1g3 = FactoryGirl.create :hanzi_char, box: @box_3, groups: [@group3]

      @box_1.reload
      @box_2.reload
      @box_3.reload
    end


    describe 'promote' do
      it 'should promote the box on the named records and adjust the box counter cache accoringly' do
        # Given boxes with the following counter cache
        @box_1.hanzi_chars_count.should == 3
        @box_2.hanzi_chars_count.should == 3

        # When I promote the two of the records in box 1
        HanziChar.promote( [ @hc1_g1.id, @hc2_g1g2.id ] )

        # they should now belong to box 2....
        @hc1_g1.reload.box.should == @box_2
        @hc2_g1g2.reload.box.should == @box_2

        # and the counter cache hsould be altered accordingly
        @box_1.reload.hanzi_chars_count.should == 1
        @box_2.reload.hanzi_chars_count.should == 5
      end


      it 'should not promote chars in box 5' do
        # given a record in box 5
        @box_5  = FactoryGirl.create :box_5
        @hc10_g1 = FactoryGirl.create :hanzi_char, box: @box_5, groups: [@group1]
        @box_5.reload

        # with a counter cache of 1
        @box_5.hanzi_chars_count.should == 1

        # When I promote this and others
        HanziChar.promote( [ @hc4_g3.id, @hc5_g1g2.id, @hc10_g1.id ])

        # the card in box 5 would still be in box 5 and the counter cache unaltered
        @hc10_g1.reload.box.should == @box_5
        @box_5.hanzi_chars_count.should == 1
      end

    end



    describe '.num_per_box' do

      it 'should get a count of characters for all boxes' do
        HanziChar.num_per_box.should == { 1 => 3, 2 => 3, 3 => 3}
      end
        
      it 'should get a count just for the group ids we give it' do
        HanziChar.num_per_box( [ @group1.id ] ).should == {1=>3, 2=>2, 3=>1}
        HanziChar.num_per_box( [ @group1.id, @group2.id ] ).should == {1=>3, 2=>2, 3=>1}
      end
    end
 

    describe '.ids_by_box_id' do
      it 'should return a hash of the record ids only in the selected groups' do
        group_ids = [ @group1.id, @group2.id ]
        expected = { 
          1 => [ @hc1_g1.id, @hc2_g1g2.id, @hc3_g1g2.id ], 
          2 => [ @hc4_g3.id, @hc5_g1g2.id, @hc6_g1g3.id ], 
          3 => [ @hc7_g1g3.id, @hc8_g1g3.id, @hc9_g1g3.id ] 
        }
        HanziChar.ids_by_box_id(group_ids).should == expected
      end
    end

  end

end
