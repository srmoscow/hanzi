require 'spec_helper'


describe Flashcard do


  describe '.new' do
    it 'should create a new flashcard' do
      fc = Flashcard.new(23, :chinese, 22)
      fc.hanzi_char_id.should == 23
      fc.display_side.should == :chinese
      fc.status.should == :untested
      fc.sequence_id.should == 22
    end
  end



  # describe '.instantiate_triplet' do
  #   it 'should return 3 flash cards one for each display side' do
  #     Random.should_receive(:rand).with(500).and_return(432, 185, 79)
  #     cards = Flashcard.instantiate_triplet(34)
  #     chinese, english, pinyin = cards

  #     chinese.hanzi_char_id.should == 34
  #     chinese.display_side.should == :chinese
  #     chinese.status.should == :untested
  #     chinese.sequence_id.should == 432

  #     english.hanzi_char_id.should == 34
  #     english.display_side.should == :english
  #     english.status.should == :untested
  #     english.sequence_id.should == 185

  #     pinyin.hanzi_char_id.should == 34
  #     pinyin.display_side.should == :pinyin
  #     pinyin.status.should == :untested
  #     pinyin.sequence_id.should == 79
  #   end
  # end


  describe '#serialize' do
    it 'should serialize the flashcard into a string' do
      fc = Flashcard.new(23, :chinese, 485)
      fc.serialize.should == '23:CU:485'
    end
  end



  describe '.deserialize' do
    it 'should instantiate a flashcard from a serialized string' do
      card = Flashcard.deserialize('584:PT:24')
      card.hanzi_char_id.should == 584
      card.display_side.should == :pinyin
      card.status.should == :true
      card.sequence_id.should == 24
    end
  end


  # describe 'sorting' do
  #   it 'should sort flashcards by sequence id' do
  #     Random.should_receive(:rand).with(500).and_return(432, 185, 79, 122, 456, 2, 98, 76, 14)
  #     array = Flashcard.instantiate_triplet(2) + Flashcard.instantiate_triplet(10) + Flashcard.instantiate_triplet(12)
  #     array.sort.map(&:sequence_id).should == [2, 14, 76, 79, 98, 122, 185, 432, 456]
  #   end
  # end


end
