# == Schema Information
#
# Table name: groups
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Group do
  
  describe '.all_ids' do
    it 'should return an empty array when there are no group records' do
      Group.all_ids.should == []
    end

    it 'should return an array of ids for all records in the database' do
      g1 = FactoryGirl.create :group
      g2 = FactoryGirl.create :group
      g3 = FactoryGirl.create :group

      Group.count.should == 3
      Group.all_ids.should == [g1.id, g2.id, g3.id]

    end
  end


end
