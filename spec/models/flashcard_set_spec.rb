# == Schema Information
#
# Table name: flashcard_sets
#
#  id               :integer          not null, primary key
#  num_cards        :integer
#  num_tested       :integer
#  groups           :string(255)
#  serialized_cards :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'spec_helper'

describe FlashcardSet do


  let(:hc1)             { FactoryGirl.create :hanzi_char }
  let(:hc2)             { FactoryGirl.create :hanzi_char }
  
  before(:each) do
    @default_group = FactoryGirl.create :default_group
  end

  describe '.new_from_ids' do

    it 'should intantiate a set of cards' do
      set = FlashcardSet.new_from_ids( [hc1.id, hc2.id] )
      set.num_cards.should == 6
      # set.groups.should == [ @default_group ]
      set.cards.each { |card| card.should be_instance_of(Flashcard) }
      set.cards.map(&:sequence_id).should == [ 0, 1, 2, 3, 4, 5 ]
    end      
  end

  describe 'saving to database' do
    it 'should successfully serialize so that record can re read and flashcards re-constituted' do
      set = FlashcardSet.new_from_ids( [hc1.id, hc2.id] )
      set.save!

      set_2 = FlashcardSet.find(set.id)
      set_2.should == set
      set_2.cards.each { |card| card.should be_instance_of(Flashcard) }
    end
  end


  describe '#next_card' do
    it 'should return the card indexed by the num_tested field' do
      set = FlashcardSet.new_from_ids( [hc1.id, hc2.id] )
      card_0 = set.cards[0]
      card_4 = set.cards[4]
      set.num_tested.should == 0
      set.next_card.should == card_0
      set.num_tested = 4
      set.next_card.should == card_4
    end



    describe '#record_result' do
      it 'should store the card, increment the num_tested and save the set' do
        set = FlashcardSet.new_from_ids( [hc1.id, hc2.id] )
        set.save!
        card_0 = set.next_card
        set.record_result(card_0, :true)

        found_set = FlashcardSet.find(set.id)
        found_set.num_tested.should == 1
        card = found_set.cards[0]
        card.status.should == :true
        card.sequence_id.should == 0
        card.hanzi_char_id.should == card_0.hanzi_char_id
      end
    end


  end

  
end
