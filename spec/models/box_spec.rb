# == Schema Information
#
# Table name: boxes
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  percentage_to_take :integer
#  hanzi_chars_count  :integer          default(0)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'spec_helper'

describe Box do

  let(:setup) {
    @box_1 = FactoryGirl.create :box_1
    @box_2 = FactoryGirl.create :box_2
    @box_3 = FactoryGirl.create :box_3
    @box_4 = FactoryGirl.create :box_4
    @box_5 = FactoryGirl.create :box_5

    @hc_1 = FactoryGirl.create :hanzi_char
    @hc_2 = FactoryGirl.create :hanzi_char
    @hc_3 = FactoryGirl.create :hanzi_char
    @hc_4 = FactoryGirl.create :hanzi_char, :box_id => 2
    @hc_5 = FactoryGirl.create :hanzi_char, :box_id => 3
  }
  
  describe '.update_counter_cache' do
    it 'should update the counter cache' do
      setup
      @box_1.hanzi_chars_count.should == 0
      @box_2.hanzi_chars_count.should == 0
      @box_3.hanzi_chars_count.should == 0

      Box.update_counter_cache!

      @box_1.reload.hanzi_chars_count.should == 3
      @box_2.reload.hanzi_chars_count.should == 1
      @box_3.reload.hanzi_chars_count.should == 1
    end
  end



  describe 'counter cache' do
    it 'should automatically change the couter cache when I change the box id' do
      setup
      Box.update_counter_cache!
      @box_1.reload.hanzi_chars_count.should == 3
      @box_2.reload.hanzi_chars_count.should == 1
      @box_3.reload.hanzi_chars_count.should == 1

      @hc_1.box = @box_2
      @hc_1.save!

      @box_1.reload.hanzi_chars_count.should == 2
      @box_2.reload.hanzi_chars_count.should == 2
      @box_3.reload.hanzi_chars_count.should == 1
    end
  end

  describe '.hanzi_chars_per_box' do
    it 'should return a hash of the count of hanzi chars for each box' do
      setup
      Box.hanzi_chars_per_box.should == { 1 => 3, 2 => 1, 3 => 1, 4=> 0, 5 => 0 }
    end
  end


  describe '!total_drawn' do
    it 'should return the sum of all the cards in the hash so far' do
      box = FactoryGirl.build :box_1
      box.send(:total_drawn, {1 => 3, 2 => 1, 3 => 1, 4=> 0, 5 => 0 } ).should == 5
      box.send(:total_drawn, {1 => 0, 2 => 0, 3 => 0, 4=> 0, 5 => 0 } ).should == 0
    end
  end


  describe '!get_quantities_per_box' do
    it 'should pick the required number of cards from each box if there are enough in each box' do
      ideal_nos_from_each_box = {1=>7, 2=>5, 3=>4, 4=>3, 5=>1}
      actual_nos_in_each_box  = {1=>7, 2=>5, 3=>4, 4=>3, 5=>1}
      Box.send(:get_quantities_per_box, ideal_nos_from_each_box, actual_nos_in_each_box).should == ideal_nos_from_each_box
    end


    it 'should top up missing cards in box 2 and 3 from box 1' do
      # given that there are not enough cards in boxes 2 and 3 to satisfy the ideal requirements
      ideal_nos_from_each_box = {1=>7, 2=>5, 3=>4, 4=>3, 5=>1}
      actual_nos_in_each_box  = {1=>15, 2=>2, 3=>2, 4=>3, 5=>1}

      # then it should pick up the extra ones from box 1
      expected = {1=>12, 2=>2, 3=>2, 4=>3, 5=>1}
      Box.send(:get_quantities_per_box, ideal_nos_from_each_box, actual_nos_in_each_box).should == expected
    end


    it 'should top up missing cards in box 1 from box 2 and 3' do
      # given that there are not enough cards in boxes 1 to satisfy the ideal requirements
      ideal_nos_from_each_box = {1=>7, 2=>5, 3=>4, 4=>3, 5=>1}
      actual_nos_in_each_box  = {1=>2, 2=>8, 3=>15, 4=>20, 5=>20}

      # then it should pick up the extra ones from boxes 2 and 3
      expected = {1=>2, 2=>8, 3=>6, 4=>3, 5=>1}
      Box.send(:get_quantities_per_box, ideal_nos_from_each_box, actual_nos_in_each_box).should == expected
    end


    it 'should take missing cards from boxes 4 and 5 if there are none in 1 2 and 3' do
       # given that there are not enough cards in boxes 1 to satisfy the ideal requirements
      ideal_nos_from_each_box = {1=>7, 2=>5, 3=>4, 4=>3, 5=>1}
      actual_nos_in_each_box  = {1=>0, 2=>0, 3=>0, 4=>20, 5=>20}

      # then it should pick up the extra ones from boxes 2 and 3
      expected = {1=>0, 2=>0, 3=>0, 4=>19, 5=>1}
      Box.send(:get_quantities_per_box, ideal_nos_from_each_box, actual_nos_in_each_box).should == expected
    end

  end


  describe '.random_ids_from_box(ids, quantity_to_pick)' do
    it 'should return the complete set if the quantity to pick is the same size as the set' do
      Box.send(:random_ids_from_box, [4,8,13,24,55], 5).should == [4,8,13,24,55]
    end


    it 'should return the required number of ids without picking the same one twice' do
      result = Box.send(:random_ids_from_box, [4, 8, 13, 24, 55, 24, 65, 2, 44], 8)
      result.size.should == 8
      result.uniq.size.should == 8
    end

  end



  describe '!calculate_ideal_nos_from_each_box' do
    it 'should return the ideal nos from each box' do
      Box.send(:calculate_ideal_nos_from_each_box, 20).should == {1 => 7, 2 => 5, 3 => 4, 4=> 3, 5 => 1 }
    end

    it 'should apportion missing cards from boxes 1 - 5 if rounding gives an shortfall' do
      expected_results = {
          20 =>  {1=>7, 2=>5, 3=>4, 4=>3, 5=>1},
          21 =>  {1=>7, 2=>6, 3=>4, 4=>3, 5=>1},
          22 =>  {1=>7, 2=>6, 3=>5, 4=>3, 5=>1},
          23 =>  {1=>7, 2=>6, 3=>6, 4=>3, 5=>1},
          24 =>  {1=>8, 2=>7, 3=>5, 4=>3, 5=>1},
          25 =>  {1=>8, 2=>7, 3=>5, 4=>3, 5=>2},
          26 =>  {1=>8, 2=>7, 3=>6, 4=>3, 5=>2},
          27 =>  {1=>9, 2=>7, 3=>5, 4=>4, 5=>2},
          28 =>  {1=>9, 2=>7, 3=>6, 4=>4, 5=>2},
          29 =>  {1=>9, 2=>8, 3=>6, 4=>4, 5=>2},
          30 =>  {1=>10, 2=>8, 3=>6, 4=>4, 5=>2},
          31 =>  {1=>10, 2=>8, 3=>7, 4=>4, 5=>2},
          32 =>  {1=>10, 2=>9, 3=>7, 4=>4, 5=>2},
          33 =>  {1=>10, 2=>9, 3=>8, 4=>4, 5=>2},
          34 =>  {1=>11, 2=>9, 3=>7, 4=>5, 5=>2},
          35 =>  {1=>11, 2=>9, 3=>8, 4=>5, 5=>2},
          36 =>  {1=>11, 2=>10, 3=>8, 4=>5, 5=>2},
          37 =>  {1=>12, 2=>10, 3=>8, 4=>5, 5=>2},
          38 =>  {1=>12, 2=>10, 3=>8, 4=>5, 5=>3},
          39 =>  {1=>12, 2=>10, 3=>9, 4=>5, 5=>3},
          40 =>  {1=>13, 2=>10, 3=>8, 4=>6, 5=>3}
      }
      expected_results.each do |total_cards_to_draw, expected_result|
        result = Box.send(:calculate_ideal_nos_from_each_box, total_cards_to_draw)
        result.should == expected_result
        result.values.sum.should == total_cards_to_draw         # just to check that our expectations are correct
      end
    end
  end


  describe '.pick_hanzi_chars' do
    it 'should randomly pick the specified number of chars from each box' do

      # Given the following ids per box...
      char_ids_per_box = {
        1 => [1,2,3,4,5,6,21,22,24],
        2 => [7,8,9,10,11,12,13,25],
        3 => [14,15,16,17],
        4 => [18,30,31],
        5 => [19,50,51,52]
      }

      # ... and the following group_ids
      group_ids = [1,2,3]

      # ... and a requirement to take this many cards from each box

      box_quantities = { 1 => 4, 2 => 6, 3 => 2, 4 => 3, 5 => 1}
      HanziChar.should_receive(:ids_by_box_id).with(group_ids).and_return(char_ids_per_box)

      # when I call pick_hanzi_chars

      selected_ids = Box.send(:pick_hanzi_chars, box_quantities, group_ids)

      # I should get a random selection of the correct amount of ids from each box
      selected_ids.size.should == box_quantities.values.sum
      selected_ids.select{|x| char_ids_per_box[1].include?(x) }.size.should == box_quantities[1]
      selected_ids.select{|x| char_ids_per_box[2].include?(x) }.size.should == box_quantities[2]
      selected_ids.select{|x| char_ids_per_box[3].include?(x) }.size.should == box_quantities[3]
      selected_ids.select{|x| char_ids_per_box[4].include?(x) }.size.should == box_quantities[4]
      selected_ids.select{|x| char_ids_per_box[5].include?(x) }.size.should == box_quantities[5]

    end
  end


  describe '.get_flashcard_ids' do
    it 'should call the methods to select the correct cards' do
      total_cards_to_draw     = 30
      hanzi_chars_per_box     = {1 => 10, 2 => 4, 3 => 16, 4 => 0, 5 => 0}
      group_ids               = [13, 16, 12]
      hanzi_chars_per_box     = double "Hanzi Chars Per Box"
      ideal_nos_from_each_box = double 'ideal_nos_from_each_box'
      box_quantities          = double 'box_quantities'
      hanzi_chars             = double 'hanzi_chars'
      
      HanziChar.should_receive(:num_per_box).with(group_ids).and_return(hanzi_chars_per_box)
      Box.should_receive(:calculate_ideal_nos_from_each_box).with(total_cards_to_draw).and_return(ideal_nos_from_each_box)
      Box.should_receive(:get_quantities_per_box).with(ideal_nos_from_each_box, hanzi_chars_per_box).and_return(box_quantities)
      Box.should_receive(:pick_hanzi_chars).with(box_quantities, group_ids).and_return(hanzi_chars)

      Box.get_flashcard_ids(total_cards_to_draw, group_ids).should == hanzi_chars


    end
  end


end




