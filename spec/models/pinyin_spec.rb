# encoding: UTF-8

require 'spec_helper'

describe Pinyin do

  let(:pinyin) { Pinyin.new('ma') }


  describe '#to_s' do
    it 'should return a transformed string' do
      pinyin = Pinyin.new('wov shi4 gong- cheng2 shi1')
      pinyin.to_s.should == 'wǒ shì gōng chéng shī'
    end

    it 'should return a string without pseudo diacritics as-is' do
      pinyin = Pinyin.new('wo shi gong cheng shi')
      pinyin.to_s.should == 'wo shi gong cheng shi'
    end

    it 'should return an already accented string as-is' do
      pinyin = Pinyin.new('wǒ shì gōng chéng shī')
      pinyin.to_s.should == 'wǒ shì gōng chéng shī'
    end
  end





  

  describe '!transform' do

    it 'should pass through single syllables without pseudo diacritics unchanged' do
      pinyin.send(:transform, 'ban').should == 'ban'
    end

    it 'should pass through multiple syllables without pseudo diacritics unchanged' do
      pinyin.send(:transform, 'wo hen hao').should == 'wo hen hao'
    end


    it 'should transform single syllables with diacritics' do
      syllables = {   
        'wo2'    => 'wó',
        'chong1' => 'chōng', 
        'cheng4' => 'chèng',
        'shi3'   => 'shǐ',
        'ba'     => 'ba',
        'ban-'   => 'bān',
        'peng/'  => 'péng',
        'pong\\' => 'pòng', 
        'fangv'  => 'fǎng' 
      }
      syllables.each do |pseudo_text, pinyin_text|
        pinyin.send(:transform, pseudo_text).should == pinyin_text
      end
    end

    it 'should return muliple syllables with numeric diacritics' do
      pinyin.send(:transform, 'wo3 shi4 gong1 cheng2 shi1').should == 'wǒ shì gōng chéng shī'
    end

    it 'should return muliple syllables with symbolic diacritics' do
      pinyin.send(:transform, "wov shi\\ gong- cheng/ shi-").should == 'wǒ shì gōng chéng shī'
    end

  end



  describe '!has_any_pseudo_diacritics' do
    it 'should return false if no syllables end with a pseudo diacritic' do
      syllables = %w{ wo hen hao }
      pinyin.send(:has_any_pseudo_diacritics?, syllables).should be_false
    end

    it 'should return true if any syllables has a psudo diacritic' do
      syllables = %w{ wo hen3 hao }
      pinyin.send(:has_any_pseudo_diacritics?, syllables).should be_true
    end
  end


  describe '!has_pseudo_diacritic?' do
    it 'should return false if no pseudo diacritic' do
      pinyin.send(:has_pseudo_diacritic?, 'hou').should be false
    end

    it 'should return true if any of the pseudo diacritics are used to end a syllable' do
      syllables = [ 'ma1', 'ma2', 'ma3', 'ma4', 'ma-', 'ma/', 'ma\\', 'mav' ]
      syllables.each do |syllable|
        pinyin.send(:has_pseudo_diacritic?, syllable).should be_true
      end
    end
  end


  describe '!parse_syllable' do
    it 'should return initials, vowels and finals' do 
      syllables = {
        'ba'       => ['b', 'a', '', 0],
        'zhang1'   => ['zh', 'a', 'ng', 1],
        'choung\\' => ['ch', 'ou', 'ng', 4],
        'zhev'     => ['zh', 'e', '', 3]
      }
      syllables.each do |syllable, parsed_syllable|
        pinyin.send(:parse_syllable, syllable).should == parsed_syllable
      end
    end


    it 'should raise if pinyin invalid' do
      expect {
        pinyin.send(:parse_syllable, 'how3')
      }.to raise_error ArgumentError, "Invalid pinyin: 'how3'"
    end

  end



  describe '!apply diacritic' do
    it 'should return the vowel with the correct diacritic applied' do
      vowels = {
        ['a', 1]   => 'ā',
        ['ao', 0]  => 'ao',
        ['uai', 2] => 'uái',
        ['uo', 3]  => 'uǒ',
        ['i', 4]   => 'ì'
      }
      vowels.each do | vowel_and_numeric_diacritic, accented_vowel | 
        vowel, numeric_diacritic = vowel_and_numeric_diacritic
        pinyin.send(:apply_diacritic, vowel, numeric_diacritic).should == accented_vowel
      end
    end


    it 'should raise if an invalid pinyin vowel is supplied' do
      expect {
        pinyin.send(:apply_diacritic, 'uaie', 3)
      }.to raise_error ArgumentError, "Invalid pinyin vowel: 'uaie'"

    end
  end


end





