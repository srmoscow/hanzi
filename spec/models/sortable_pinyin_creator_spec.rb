# encoding: UTF-8

require 'spec_helper'


describe SortablePinyinCreator do

  it 'should create a sortable pinyin word from one pinyin word' do
    SortablePinyinCreator.new('lǎo').create.should == 'lao3'
  end

  it 'should create a sentence of sortable pinyin workds from a sentence of pinyin words' do
    orig = 'fāng lǎo shī de shéng rì shì míng tiān'
    SortablePinyinCreator.new(orig).create.should == 'fang1 lao3 shi1 de sheng2 ri4 shi4 ming2 tian1'
  end


end