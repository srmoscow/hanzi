$:<< File.join(File.dirname(__FILE__), '..')
require "bundler/capistrano" 
require 'caplock'
require 'rvm/capistrano'




### GIT configuration

set :keep_releases, 5 
set :scm, :git
set :application, 'hanzi'
# set :repository, "https://srmoscow@bitbucket.org/srmoscow/hanzi.git"
set :repository, "ssh://git@bitbucket.org/srmoscow/hanzi.git"
set :branch,  'master'
set :deploy_via, :remote_cache
set :scm_user, "srmoscow"
# ssh_options[:forward_agent] = true
set :ssh_options, { :forward_agent => true }



# RVM configuration

set :rvm_ruby_string,   'ruby-1.9.3-p327-falcon@hanzi'      # use the same ruby as used locally for deployment
set :rvm_autolibs_flag, "read-only"                         # more info: rvm help autolibs
set :rvm_type, :system
set :rvm_install_with_sudo, true
before 'deploy:setup',  'rvm:install_rvm'         # install RVM
before 'deploy:setup',  'rvm:install_ruby'        # install Ruby and create gemset, OR:
before 'deploy:setup',  'rvm:create_gemset'       # only create gemset


# Deployment configuration


set :deploy_to ,"/var/www/#{application}"
role :web, "dohanzi.stephenrichards.eu"                           # Your HTTP server, Apache/etc
role :app, "dohanzi.stephenrichards.eu"                           # This may be the same as your `Web` server
role :db,  "dohanzi.stephenrichards.eu", :primary => true         # This is where Rails migrations will run
# role :db,  "your slave db-server here"
set :use_sudo, false
set :user, 'deploy'
default_run_options[:pty] = true







after 'deploy:update_code', 'deploy:migrate'

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end



