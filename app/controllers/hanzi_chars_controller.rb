class HanziCharsController < ApplicationController

  before_filter :check_for_cancel, :only => [:create, :update]


  def index
    pinyin_start_char = params[:pinyin]
    group_id = extract_group_id(params)
    @hanzis = HanziChar.all_by_pinyin(pinyin_start_char, group_id).page(params[:page]).per(20)
    @groups_array = Group.all_by_name.map { |group| [group.name, group.id] }
    @groups_array.unshift ['All Groups', nil] 
  end

  def show
    @hanzi = HanziChar.find(params[:id])
  end

  def new
    @hanzi = HanziChar.new
    @groups = Group.all_by_name
  end

  def create
    begin
      if @hanzi = HanziChar.create!(params[:hanzi_char])
        flash[:notice] = "Character created: #{@hanzi}"
        redirect_to hanzi_chars_path
      else
        flash[:error] = 'Unable to create character'
        redirect_to new_hanzi_char_path
      end
    rescue => err
      flash[:error] = err.message
      redirect_to new_hanzi_char_path
    end
  end


  def edit
    @hanzi = HanziChar.find(params[:id])
    @groups = Group.all
  end

  def update
    @hanzi = HanziChar.find(params[:id])
    @hanzi.update_attributes(params[:hanzi_char])
    flash[:notice] = "Character Updated: #{@hanzi}"
    redirect_to hanzi_chars_path
  end

  def destroy
  end






  private


  def extract_group_id(params)
    return nil unless params.has_key?(:group)
    return nil if params[:group][:group_id].blank?
    return params[:group][:group_id].to_i
  end


  def check_for_cancel
    if params[:commit] == "Cancel"
      redirect_to hanzi_chars_path
    end
  end

end
