class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :determine_revision

  # Returns the template filename for the current controller / action
  #
  def template_filename(action)
    @@template_filenames = {} unless defined?(@@template_filenames)
    if @@template_filenames[self.class.to_s].nil?
      @@template_filenames[self.class.to_s] = {}
    end

    if @@template_filenames[self.class.to_s][action].nil?    
      pattern = "#{Rails.root}/app/views/#{self.class.to_s.underscore.sub(/_controller$/, '')}/#{action}*"
      @@template_filenames[self.class.to_s][action] = Dir[pattern].first
    end
    @@template_filenames[self.class.to_s][action]
  end

  private 

  def determine_revision
    if File.exist?("#{Rails.root}/REVISION")
      @revision = File.open("#{Rails.root}/REVISION") do |fp|
        fp.readline[0..7]
      end
    else
      @revision = 'Unversioned'
    end
  end

end
