class FlashcardSetsController < ApplicationController


  # before_filter :check_for_cancel, :only => [:create, :update]


  def index
    # @hanzis = HanziChar.all_by_pinyin
  end


  # this method is responsible for showing whichever card is next and asking for the answer.
  def show
    @flashcard_set = FlashcardSet.find(params[:id])
    @flashcard = @flashcard_set.next_card
    if @flashcard.nil?
      wrong_characters = @flashcard_set.update_boxes
      if wrong_characters.nil?
        flash[:notice] = 'Well done! All characters correctly remembered.'
      else
        flash[:warning] = ['You need to revise the following characters:'] + wrong_characters
      end
      redirect_to hanzi_chars_path
    end
  end


  
  def record_result
    @flashcard_set = FlashcardSet.find(params[:id])
    @flashcard = @flashcard_set.next_card
    @flashcard_set.record_result(@flashcard, transform_result(params[:result]))
    redirect_to flashcard_set_path
  end




  def new
    @flashcard_set = FlashcardSet.new
  end


 #  {"utf8"=>"✓",
 # "authenticity_token"=>"YU+r9OrY5Kf7yuA+h8gXj7wjXlgq88AYYXl1l/CX3RI=",
 # "flashcard_set"=>{"num_cards"=>"20", "group_ids"=>["13", ""]},
 # "commit"=>"Create Flashcard set",
 # "action"=>"create",
 # "controller"=>"flashcard_sets"}
  def create
    puts "\n++++++++ CREATE ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    creator =  FlashcardSetCreator.new(params[:flashcard_set])
    if creator.valid?
      @flashcard_set = creator.set
      redirect_to flashcard_set_path(@flashcard_set)
    else
      flash[:error] = creator.errors
      redirect_to hanzi_chars_path
    end
  end



  private

  def transform_result(result)
    case result
    when "0"
      :false
    when "1"
      :true
    else
      raise "Unexpected result: #{result.inspect}"
    end
  end

end





