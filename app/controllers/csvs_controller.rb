class CsvsController < ApplicationController




  def create
    send_data HanziChar.to_csv, :filename => "hanzi_chars_#{Time.now.strftime('%Y%m%d_%H%M%S')}.csv"
    # redirect_to hanzi_chars_path
  end

end
