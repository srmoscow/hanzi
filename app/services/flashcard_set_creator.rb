


class FlashcardSetCreator


  attr_reader :errors, :set


  # Instantiates a FlashcardSetCreator object with params from the controller.
  #
  # The params hash will contain:
  #
  # * num_cards
  # * group_ids 
  #
  def initialize(params)
    puts "\n++++++++ FLASHCARD SET CREATOR  ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    puts params.inspect
    puts "\n++++++++  ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    @errors = []
    @group_ids = params[:group_ids].reject(&:blank?).map(&:to_i)

    if @group_ids.empty?
      @group_ids = nil
    end

    @errors << 'You must specify a number of cards to pick' if params[:num_cards].blank?
    @num_cards = params[:num_cards].to_i unless params[:num_cards].blank?


    if valid?
      card_ids = Box.get_flashcard_ids(@num_cards, @group_ids)

      unless Rails.env.test?
        box_ids = []


        card_ids.each do |card_id|
          char = HanziChar.find card_id
          box_ids << char.box_id
        end

        puts "\n++++++++ HAVE PICKED CARDS FROM BOXES #{box_ids.inspect} ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
      end


      @set = FlashcardSet.new_from_ids(card_ids)
      @set.save!
    end
  end



  def valid?
    @errors.empty?
  end


end
