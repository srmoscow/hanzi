# encoding: UTF-8
# == Schema Information
#
# Table name: hanzi_chars
#
#  id              :integer          not null, primary key
#  character       :string(255)
#  pinyin          :string(255)
#  meaning         :text
#  strokes         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  mnemonic        :text
#  box_id          :integer          default(1)
#  sortable_pinyin :string(255)
#


class HanziChar < ActiveRecord::Base


  attr_accessible :character, :meaning, :pinyin, :strokes, :mnemonic, :group_ids, :box_id, :tone_hint

  has_many :groups, :through => :grouped_hanzi

  has_many :grouped_hanzi

  belongs_to :box, :counter_cache => true

  before_save :transform_pinyin_and_mnemonic

  validates :character, :uniqueness => true
  validates :character, :presence => true
  validates :pinyin,    :presence=> true



  # Returns all records, optionally constrained by a group id and / or starting from a certain pinyin character 
  # ordered by sortable_pinyin
  # @param [String] : pinyin_start_character : only return records with sortable pinyin equal to or greater than this character
  # @param [Fixnum] : group_id : only return records in this group, or all groups if nil
  #
  def self.all_by_pinyin(pinyin_start_char = nil, group_id = nil)
    pinyin_start_char = 'a' if pinyin_start_char.nil?
    if group_id.nil?
      recs = self.where('sortable_pinyin > ?', pinyin_start_char).includes(:groups)
    else
      recs = self.joins(:groups).where('sortable_pinyin > ?', pinyin_start_char).includes(:groups).where('groups.id' => group_id)
    end
    recs = recs.order(:sortable_pinyin)
  end



  # returns a hash of record ids, keyed by box number
  def self.ids_by_box_id(group_ids)
    hcs_hashed_by_box_id = HanziChar.select('id, box_id').group_by(&:box_id)
    ids_hashed_by_box_id = {}
    hcs_hashed_by_box_id.each { |box_id, hc_array| ids_hashed_by_box_id[box_id] = hc_array.map(&:id) }
    ids_hashed_by_box_id
  end





  def self.to_csv
    # filename = "#{ENV['HOME']}/hanzi_chars.#{Time.now.strftime('%Y%m%d_%H%M%S')}.csv"
    CSV.generate do |csv|
      csv << ["English","Traditional Chinese","Simplified Chinese","Pinyin","Box","Quiz English","Quiz Chinese","Quiz Pinyin","GroupName"]
      HanziChar.all.each do |hc|
        csv << [hc.meaning, '', hc.character, hc.pinyin, hc.box_id, 1,1,1,'']
      end
    end
    
  end



  # returns an array of ids of all the HanziChar ids in the specified_group_ids
  def self.ids_for_groups(group_ids)
    GroupedHanzi.select(:hanzi_char_id).where('group_id in (?)', group_ids).map(&:hanzi_char_id).uniq
  end




  # Returns the number of cards per box - only counting HanziChars in the specified group_ids
  # @param [Array] group_ids - Only count cards in the groups in this array.  Default is nil, meaning all groups.
  # returns Hash  keyed by box, with the value being the number of cards per box
  #
  def self.num_per_box(group_ids = nil)
    if group_ids.nil?
      hcids = HanziChar.all.map(&:id)
    else
      hcids = HanziChar.ids_for_groups(group_ids)
    end
    id_hash = HanziChar.select([:id, :box_id]).where('id in (?)', hcids).group_by(&:box_id)
    record_count_hash = {}
    id_hash.each { |key, value_array| record_count_hash[key] = value_array.size }
    record_count_hash
  end






  def self.promote(ids)
    recs = HanziChar.find(ids)
    recs.each do |rec|
      if rec.box.id != 5
        rec.box = Box.find(rec.box_id + 1)
        rec.save
      end
    end
  end



  def self.demote(ids)
    recs = HanziChar.find(ids)
    recs.each do |rec|
      if rec.box_id != 1
        rec.box Box.find(rec.box_id - 1)
        rec.save!
      end
    end
  end




  def transform_pinyin_and_mnemonic
    self.pinyin = Pinyin.new(self.pinyin).to_s
    self.sortable_pinyin = SortablePinyinCreator.new(self.pinyin).create
    self.mnemonic = EmbeddedPinyin.new(self.mnemonic).to_s unless self.mnemonic.nil?
  end


  def to_s
    self.character + ' ' + self.pinyin
  end



  def group_names
    groups.map(&:name).join(", ")
  end


  # This enables groups to be specified on the  incoming params as an array of ids
  def group_ids=(group_ids)
    group_ids.delete("")
    groups = Group.find(group_ids)
    self.groups = groups
  end


  # decorator_methods

  def decorate_mnemonic
    self.mnemonic.blank? ? "None" : self.mnemonic
  end


  def decorate_tone_hint
    self.tone_hint.blank? ? "None" : self.tone_hint
  end


  def decorate_mnemonic_present?
    self.mnemonic.blank? ? "N" : "Y"
  end


  def decorate_tone_hint_present?
    self.tone_hint.blank? ? "N" : "Y"
  end






end
