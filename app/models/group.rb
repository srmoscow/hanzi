# == Schema Information
#
# Table name: groups
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Group < ActiveRecord::Base
  attr_accessible :name

  has_many :hanzi_chars, :through => :grouped_hanzi

  has_many :grouped_hanzi

  has_and_belongs_to_many :flashcard_sets

  scope :all_by_name, order('name')

  scope :named_default, where(:name => 'Default')

  def self.all_ids
    self.all.map(&:id)
  end


end
