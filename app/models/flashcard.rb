

# This class represents a flash card in the set, and hold information about whether or not it
# shows the english, chinese or pinyin side, whether or not it has been tested, etc.
class Flashcard

  include Comparable


  attr_reader :hanzi_char_id, :display_side
  attr_accessor :status, :sequence_id


  # Use this method to instantiate three flash cards, one each for English, Chinese and Pinyin
  # def self.instantiate_triplet(hanzi_char_id)
  #   card_c = self.new(hanzi_char_id, :chinese)
  #   card_e = self.new(hanzi_char_id, :english)
  #   card_p = self.new(hanzi_char_id, :pinyin)
  #   [card_c, card_e, card_p]
  # end



  # This initialiser should only by used by this class, called from instantiate_triplet
  def initialize(hanzi_char_id, display_side, sequence_id)
    @hanzi_char_id = hanzi_char_id
    @hanzi_char    = nil
    @display_side  = display_side
    @status        = :untested
    @sequence_id   = sequence_id
  end


  def hanzi_char
    @hanzi_char ||= HanziChar.find @hanzi_char_id
  end



  def self.deserialize(string)
    hanzi_char_id, display_status, sequence_id = string.split(':')
    display_side = deserialize_display_side(display_status.first)
    card = self.new(hanzi_char_id.to_i, display_side, sequence_id.to_i)
    card.status = deserialize_status(display_status.last)
    card
  end


  def to_s
    "id:#{@hanzi_char_id};display:#{@display_side.inspect};seq:#{@sequence_id}"
  end



  def serialize
    "#{@hanzi_char_id}:#{serialize_display_side}#{serialize_status}:#{sequence_id}"
  end


  def <=>(other)
    @sequence_id <=> other.sequence_id    
  end


  def untested?
    @status == :untested
  end



  def answered_correctly?
    raise 'Card not yet tested' if untested?
    @status == :true
  end


  def answered_wrongly?
    raise 'Card not yet tested' if untested?
    @status == :false
  end



  def display
    case @display_side
    when :chinese
      hanzi_char.character
    when :pinyin
      hanzi_char.pinyin
    when :english
      hanzi_char.meaning
    end
  end



  def display_result
    case display_side
    when :chinese
      [hanzi_char.pinyin, hanzi_char.meaning]
    when :pinyin
      [hanzi_char.character, hanzi_char.meaning]
    when :english
      [hanzi_char.character, hanzi_char.pinyin]
    end
  end


  def display_mnemonic
    hanzi_char.mnemonic == "" || hanzi_char.mnemonic.nil? ? "There is no mnemonic for this card" : hanzi_char.mnemonic
  end









  private

  def serialize_display_side
    case @display_side
    when :chinese
      'C'
    when :english
      'E'
    when :pinyin
      'P'
    end
  end


  def serialize_status
    case @status
    when :untested
      'U'
    when :true
      'T'
    when :false
      'F'
    end
  end


  def self.deserialize_display_side(str)
    case str
    when 'C'
      :chinese
    when 'E'
      :english
    when 'P'
      :pinyin
    else
      raise ArgumentError.new('Invalid serialization')
    end
  end


  def self.deserialize_status(str)
    case str
    when 'U'
      :untested
    when 'T'
      :true
    when 'F'
      :false
    else
      raise ArgumentError.new('Invalid serialization')
    end
  end

end
















