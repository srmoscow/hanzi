# encoding: UTF-8

# This class is used to create sortable pinyin from existing accented pinyin

class SortablePinyinCreator


  @@accent_transform_table =  {

    'ā' => [1, 'a'],
    'á' => [2, 'a'],
    'ǎ' => [3, 'a'],
    'à' => [4, 'a'],
    'ē' => [1, 'e'],
    'é' => [2, 'e'],
    'ě' => [3, 'e'],
    'è' => [4, 'e'],
    'ī' => [1, 'i'],
    'í' => [2, 'i'],
    'ǐ' => [3, 'i'],
    'ì' => [4, 'i'],
    'ō' => [1, 'o'],
    'ó' => [2, 'o'],
    'ǒ' => [3, 'o'],
    'ò' => [4, 'o'],
    'ū' => [1, 'u'],
    'ú' => [2, 'u'],
    'ǔ' => [3, 'u'],
    'ù' => [4, 'u'],
    'ǖ' => [1, 'v'],
    'ǘ' => [2, 'v'],
    'ǚ' => [3, 'v'],
    'ǜ' => [4, 'v']
  }

  @@accented_characters = @@accent_transform_table.keys

  def initialize(accented_pinyin)
    @accented_pinyin = accented_pinyin
  end

  def create
    @result = []
    words = @accented_pinyin.split(' ')
    words.each { |word| @result  << convert_word(word) }
    @result.join(' ')
  end

  private

  def convert_word(word)
    new_word = ''
    tone = ''
    word.each_char do |char|
      if @@accented_characters.include?(char)
        tone = @@accent_transform_table[char].first
        new_word << @@accent_transform_table[char].last
      else
        new_word << char
      end
    end
    new_word << tone.to_s
    new_word
  end

end