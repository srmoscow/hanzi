# == Schema Information
#
# Table name: flashcard_sets
#
#  id               :integer          not null, primary key
#  num_cards        :integer
#  num_tested       :integer
#  groups           :string(255)
#  serialized_cards :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#



class FlashcardSet < ActiveRecord::Base
  attr_accessible :cards, :num_cards, :num_tested

  attr_accessor :cards

  serialize :groups, Array

  has_and_belongs_to_many :groups

  
  before_save :serialize_cards
  after_find  :deserialize_cards


  def serialize_cards
    serialized_cards_array = @cards.map(&:serialize)
    self.serialized_cards = serialized_cards_array.join(';')
  end


  def deserialize_cards
    serialized_cards_array = self.serialized_cards.split(';')
    @cards = serialized_cards_array.map{ |card| Flashcard.deserialize(card) }
  end


  def self.new_from_ids(hanzi_char_ids)
    set = self.new
    set.num_tested = 0
    set.generate_cards_from_ids(hanzi_char_ids)
    set
  end

 

  def generate_cards_from_ids(hanzi_char_ids)
    @cards = Array.new
    pinyin_offset = hanzi_char_ids.size
    english_offset = hanzi_char_ids.size * 2
    card_sides_set = [ [:chinese, :pinyin, :english], [:pinyin, :english, :chinese], [:english, :chinese, :pinyin] ]
    hanzi_char_ids.each_with_index do |hanzi_char_id, index|
      mod = index % 3
      card_sides = card_sides_set[mod]
      @cards << Flashcard.new(hanzi_char_id, card_sides[0], index)
      @cards << Flashcard.new(hanzi_char_id, card_sides[1], index + pinyin_offset)
      @cards << Flashcard.new(hanzi_char_id, card_sides[2], index + english_offset)
    end
    self.num_cards = @cards.size
    self.cards.sort!  
    self
  end


 



  def next_card
    self.cards[num_tested]
  end


  def record_result(card, result)
    raise ArgumentError unless [:true, :false].include?(result)
    card.status = result
    @cards[card.sequence_id] = card
    self.num_tested +=1 
    self.save!
  end


  # This method reviews the result, and updates the boxes accordingly.
  def update_boxes
    raise "Update boxes called before all cards in set have been tested" unless self.num_cards == self.num_tested
    correct_ids = []
    wrong_ids = []
    @cards.each do |card|
      card.answered_correctly? ? correct_ids << card.hanzi_char_id : wrong_ids << card.hanzi_char_id
    end
    
    # a card may have been answered correctly on one side, but wrong on the other, so we don't count it as correct
    card_ids_to_promote = correct_ids - wrong_ids
    HanziChar.promote(card_ids_to_promote) unless card_ids_to_promote.empty?
    HanziChar.demote(wrong_ids) unless wrong_ids.empty?
  end





  private

  def sequence_cards
    @cards.each_with_index do |card, index|
      card.sequence_id = index
    end
  end

  
  # returns the wrap-round index
  def wrap_around(num_cards, index)
    if index > num_cards - 1
      index = index - num_cards
    end
    index
  end



end
