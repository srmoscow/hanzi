# == Schema Information
#
# Table name: boxes
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  percentage_to_take :integer
#  hanzi_chars_count  :integer          default(0)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Box < ActiveRecord::Base
  attr_accessible :name, :percentage_to_take

  has_many :hanzi_chars


  @@box_source_percentages = {1 => 30, 2 => 25, 3 => 22, 4 => 15, 5 => 8}

  


  def self.update_counter_cache!
    self.all.each do |box|
      box.hanzi_chars_count = box.hanzi_chars.length
      box.save!
    end
  end


  def self.hanzi_chars_per_box
    result = {}
    self.all.each do |box|
      result[box.id] = box.hanzi_chars.size
    end
    result
  end


  # Returns the ids of the cards to go into the set.  
  #
  # This is calcluated from the standard percentages, taking into account the actual numbers of cards in each box of the required groups
  #
  # @param [Fixnum] total_cards_to_draw : the number of cards that should be in the set
  # @param [Hash] hanzi_chars_per_box :  a hash keyed by box ids of how many cards of the required groups exist in each box
  # @param [Array] group_ids : the ids of the eligible groups from which to draw the cards
  #
  def self.get_flashcard_ids(total_cards_to_draw, group_ids)
    puts "\n++++++++ GET FLASHCARD IDS total cards to draw: #{total_cards_to_draw}, group_ids: #{group_ids.inspect} ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    hanzi_chars_per_box = HanziChar.num_per_box(group_ids)                # returns a hash of the number of cards in each box that are of the required groups
    puts "\n++++++++ hanzi_chars_per_box #{hanzi_chars_per_box.inspect} ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    infeb = calculate_ideal_nos_from_each_box(total_cards_to_draw)        # returns the number to take from each box accorging to the percentages
    puts "\n++++++++ infeb #{infeb.inspect} ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"

    box_quantities = get_quantities_per_box(infeb, hanzi_chars_per_box)   # returns the number to take from each box taking into account the actual quantities in each box
    puts "\n++++++++ box quantities #{box_quantities.inspect} ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    hanzi_chars = pick_hanzi_chars(box_quantities, group_ids)             # returns the ids of the cards to pick, randomsly selected from the preveous step
    puts "\n++++++++ hanzi_chars #{hanzi_chars.inspect} ++++++ #{__FILE__}::#{__LINE__} +++++\n\n"
    hanzi_chars
  end








  private

  # Randomly select the specified number of cards from each box.
  # Note: this method should only be called by Box
  # @param [Hash] box_quantities : a hash detailing the number of cards to select from each box
  # @param [Array] group_ids : an Array of ids of the eligible groups from which to pick the cards
  #
  
  def self.pick_hanzi_chars(box_quantities, group_ids)
    ids_per_box = HanziChar.ids_by_box_id(group_ids)
    selected_ids = []
    ids_per_box.each do |box_no, ids|
      next if box_quantities[box_no].nil?
      selected_ids += random_ids_from_box(ids, box_quantities[box_no])
    end
    selected_ids.uniq
  end
  private_class_method :pick_hanzi_chars


  
  def self.random_ids_from_box(ids, quantity_to_pick)
    return ids if ids.size == quantity_to_pick
    result = []
    while result.size < quantity_to_pick
      random_index = Random.new.rand(0..(ids.size - 1))
      result << ids[random_index] unless result.include?(ids[random_index])
    end
    result
  end
  private_class_method :random_ids_from_box



  

  # returns the total number of cards in result
  def total_drawn(result)
    result.values.sum
  end

  # returns a hash of the number of cards to take from each box if each box were to have sufficient cards 
  
  def self.calculate_ideal_nos_from_each_box(total_cards_to_draw)
    result = {}
    @@box_source_percentages.each do |box_no, percentage|
      result[box_no] = (total_cards_to_draw * (percentage / 100.0)).to_i
    end

    # we round down on the above calculation, so there may be some cards missing; if so, top up here
    cards_missing = total_cards_to_draw - result.values.sum
    (1..cards_missing).each do |i|
      result[i] += 1
    end
    result
  end
  private_class_method :calculate_ideal_nos_from_each_box



  # This returns a hash of the actual numbers to take from each box after taking into account the number of cards in each box that are available
  
  def self.get_quantities_per_box(ideal_nos_from_each_box, hanzi_chars_per_box)
    result = {}
    remaining_cards = {}
    ideal_nos_from_each_box.each do |box_no, num_cards_to_take|
      next if hanzi_chars_per_box[box_no].nil?
      if hanzi_chars_per_box[box_no] >= num_cards_to_take
        result[box_no] = num_cards_to_take
        remaining_cards[box_no] = hanzi_chars_per_box[box_no] - num_cards_to_take
      else
        result[box_no] = hanzi_chars_per_box[box_no]
        remaining_cards[box_no] = 0
      end
    end

    # We may not have been able to get all the cards we want, so we just top them up from the remaining cards until we hit the number we want
    if result.values.sum < ideal_nos_from_each_box.values.sum
      num_cards_to_top_up = ideal_nos_from_each_box.values.sum - result.values.sum
      hanzi_chars_per_box.each do |box_no, num_chars_in_box|
        remaining_cards_in_this_box = hanzi_chars_per_box[box_no] - result[box_no]
        if remaining_cards_in_this_box >= num_cards_to_top_up
          result[box_no] += num_cards_to_top_up
          num_cards_to_top_up = 0
        else
          result[box_no] += remaining_cards_in_this_box
          num_cards_to_top_up -= remaining_cards_in_this_box
        end
        break if num_cards_to_top_up == 0
      end
    end
    result
  end
  private_class_method :get_quantities_per_box

end
