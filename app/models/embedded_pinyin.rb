# encoding: UTF-8

# This class encapsulates a text string which may or may not have embedded pinyin (signified by a preceeding colon)
# This class will extact all such pinyin words, replace them with proper pinyin and display the result when to_s is called.
#
# e.g.
#    The character 见 :jian\ represents the verb to see, and 件 （also :jian\) means item, or -ware, whereas 减 :jianv means alkali or soda.
# would be transformed to:
#    The character 见 jiàn represents the verb to see, and 件 （also :jiàn) means item, or -ware, whereas 减 :jiǎn means alkali or soda.
#


class EmbeddedPinyin


  @@pinyin_word_finding_regex = /:[a-zü1234\-\/v\\]+/            # search for pinyin workds beginnign with a colon and ending with a space or pseudo diacritic

  def initialize(text)
    @text = text
    @result = nil
    scan_for_pinyin
  end


  def to_s
    @result
  end


  private


  def scan_for_pinyin
    @result = @text
    pinyin_words = @text.scan(@@pinyin_word_finding_regex)
    return if pinyin_words.empty?

    pinyin_substitutions = make_pinyin_substitution_hash(pinyin_words)
    pinyin_substitutions.each do |original, replacement|
      @result.gsub!(original, replacement)
    end

  end



  def make_pinyin_substitution_hash(words)
    hash = {}
    words.each do |word| 
      hash[word] = Pinyin.new(word.gsub(/^:/, '')).to_s
    end
    hash
  end







end