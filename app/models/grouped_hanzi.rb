# == Schema Information
#
# Table name: grouped_hanzis
#
#  id            :integer          not null, primary key
#  hanzi_char_id :integer
#  group_id      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class GroupedHanzi < ActiveRecord::Base
  attr_accessible :character_id, :group_id

  belongs_to :group

  belongs_to :hanzi_char
end
