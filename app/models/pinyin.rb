# encoding: UTF-8

# This class is repsonsible for putting the correct accented characters on pinyin text
#
# e.g 
# * gong1 cheng2 shi1 => gōng chéng shī
# * gong- cheng/ shi- => gōng chéng shī


class Pinyin

  @@vowels = {
    'a'   => %w{  a   ā   á   ǎ   à },
    'ai'  => %w{  ai  āi  ái  ǎi  ài },
    'ao'  => %w{  ao  āo  áo  ǎo  ào },
    'e'   => %w{  e   ē   é   ě   è },
    'ei'  => %w{  ei  ēi  éi  ěi  èi },
    'i'   => %w{  i   ī   í   ǐ   ì },
    'ia'  => %w{  ia  iā  iá  iǎ  ià },
    'iao' => %w{  iao iāo iáo iǎo iaò },
    'ie'  => %w{  ie  iē  ié  iě  iè },
    'io'  => %w{  io  iō  ió  iǒ  iò },
    'iu'  => %w{  iu  iū  iú  iǔ  iù },
    'o'   => %w{  o   ō   ó   ǒ   ò },
    'ou'  => %w{  ou  ōu  óu  ǒu  òu },
    'u'   => %w{  u   ū   ú   ǔ   ù },
    'ua'  => %w{  ua  uā  uá  uǎ  uà },
    'uai' => %w{  uai uāi uái uǎi uài },
    'ue'  => %w{  ue  uē  ué  uě  uè },
    'uo'  => %w{  uo  uō  uó  uǒ  uò },
    'ui'  => %w{  ui  uī  uí  uǐ  uì },
    'ü'   => %w{  u   ǖ   ǘ   ǚ   ǜ },
    'üe'  => %w{  ue  üē  üé  üě  üè },
    'üa'  => %w{  ua  üā  üá  üǎ  üà }
  }

  @@valid_finals = [ '', 'n', 'ng', 'r']



  def initialize(pseudo_text)
    @text = transform(pseudo_text)
  end


  def to_s
    @text
  end


  private


  def transform(pseudo_text)
    syllables = pseudo_text.split(' ')
    unless has_any_pseudo_diacritics?(syllables)
      return pseudo_text
    end
    transformed_syllables = []
    syllables.each { |syl| transformed_syllables << transform_syllable(syl) }
    transformed_syllables.join(' ')
  end



  def has_any_pseudo_diacritics?(syllables)
    syllables.each do |syllable|
      return true if has_pseudo_diacritic?(syllable)
    end
    return false
  end


  def has_pseudo_diacritic?(syllable)
    return true if syllable =~ /.+[1234\-\/\\v]$/
    return false
  end


  def transform_syllable(syllable)
    return syllable unless has_pseudo_diacritic?(syllable)
    initial, vowels, finals, diacritic = parse_syllable(syllable)
    accented_vowels = apply_diacritic(vowels, diacritic)
    initial + accented_vowels + finals

  end



  def parse_syllable(syllable)
    result = syllable =~ /^([bcdfghjklmnpqrstvwxyz]{0,2})([aeiouü]{1,3})([ngr]{0,2})([1234\-\/\\v]?)$/
    raise ArgumentError.new("Invalid pinyin: '#{syllable}'") if result != 0
    diacritic = $4
    if diacritic == ''
      diacritic = 0
    else
      diacritic = diacritic.tr('-/v\\', '1234').to_i
    end

    [$1, $2, $3, diacritic]
  end


  def apply_diacritic(vowels, diacritic)
    raise ArgumentError.new("Invalid pinyin vowel: '#{vowels}'") unless @@vowels.keys.include?(vowels)
    @@vowels[vowels][diacritic]
  end




end